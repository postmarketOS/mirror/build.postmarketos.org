bpo.api package
===============

Submodules
----------

bpo.api.job_callback.build_image module
---------------------------------------

.. automodule:: bpo.api.job_callback.build_image
   :members:
   :undoc-members:
   :show-inheritance:

bpo.api.job_callback.build_package module
-----------------------------------------

.. automodule:: bpo.api.job_callback.build_package
   :members:
   :undoc-members:
   :show-inheritance:

bpo.api.job_callback.get_depends module
---------------------------------------

.. automodule:: bpo.api.job_callback.get_depends
   :members:
   :undoc-members:
   :show-inheritance:

bpo.api.job_callback.repo_bootstrap module
------------------------------------------

.. automodule:: bpo.api.job_callback.repo_bootstrap
   :members:
   :undoc-members:
   :show-inheritance:

bpo.api.job_callback.sign_index module
---------------------------------------

.. automodule:: bpo.api.job_callback.sign_index
   :members:
   :undoc-members:
   :show-inheritance:

bpo.api.public.update_job_status module
---------------------------------------

.. automodule:: bpo.api.public.update_job_status
   :members:
   :undoc-members:
   :show-inheritance:

bpo.api.push_hook.gitlab module
-------------------------------

.. automodule:: bpo.api.push_hook.gitlab
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: bpo.api
   :members:
   :undoc-members:
   :show-inheritance:
