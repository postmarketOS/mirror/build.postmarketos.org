Welcome to the bpo (build.postmarketos.org) documentation!
==========================================================

bpo (short-form of build.postmarketos.org) is the central builder for images for postmarketOS development.

For the latest releases please check the `repository`_.

In case of any problems that is also the place to check the `issue-tracker`_.

For further information, please check out the `postmarketOS-wiki`_.


.. toctree::
   :maxdepth: 3
   :caption: Contents:

   database
   api/modules
   helpers/modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


*Note:* This documentation is currently a work-in-progress, your feedback and contributions are very welcome!

.. _postmarketOS-wiki: https://wiki.postmarketos.org/wiki/Main_Page
.. _issue-tracker: https://gitlab.postmarketos.org/postmarketOS/build.postmarketos.org/-/issues
.. _repository: https://gitlab.postmarketos.org/postmarketOS/build.postmarketos.org/
