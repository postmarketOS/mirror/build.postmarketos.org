# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: AGPL-3.0-or-later
import re

def get_ui_list(chassis, supports_gpu=True, exclude_ui=[], add_ui=[]):
    ui = set()

    if "handset" in chassis:
        if supports_gpu:
            ui.add("gnome-mobile")
            ui.add("phosh")
            ui.add("plasma-mobile")
            ui.add("sxmo-de-sway")
        else:
            ui.add("mate")
            ui.add("xfce4")
            ui.add("sxmo-de-dwm")

    if "convertible" in chassis or "tablet" in chassis:
        if supports_gpu:
            ui.add("phosh")
            ui.add("sxmo-de-sway")
        else:
            ui.add("mate")
            ui.add("xfce4")
            ui.add("sxmo-de-dwm")

    if "convertible" in chassis or "laptop" in chassis:
        ui.add("console")
        if supports_gpu:
            ui.add("gnome")
            ui.add("plasma-desktop")
            ui.add("sway")
        else:
            ui.add("mate")
            ui.add("xfce4")
            ui.add("i3wm")

    if "embedded" in chassis:
        ui.add("console")

    if exclude_ui:
        for ui_to_remove in exclude_ui:
            ui.remove(ui_to_remove)

    if add_ui:
        for ui_to_add in add_ui:
            ui.add(ui_to_add)

    return list(ui)

# Regular expressions for resulting dir and file names
pattern_dir = re.compile("^[0-9]{8}-[0-9]{4}$")
pattern_file = re.compile(
        "^[0-9]{8}-[0-9]{4}-postmarketOS-[a-z0-9._+-]+(\\.img\\.xz|\\.zip)"
        "(\\.sha(256|512))?$")

# Default password for images
password = "147147"

# Branches to build images for, can be overridden per device in 'images' below
branches_default = [
        "master",
        "v24.12",
    ]

# Prevent errors by listing explicitly allowed UIs here. Notably "none" is
# missing, as the UI does not follow the usual naming scheme
# (postmarketos-ui-none/APKBUILD doesn't exist). Code in bpo.jobs.build_image
# would try to extract the pkgver from the file and do something undefined.
# Use "console" instead.
ui_list = {
    "asteroid": "Asteroid",
    "console": "Console",
    "fbkeyboard": "Fbkeyboard",
    "gnome": "GNOME",
    "gnome-mobile": "GNOME Mobile",
    "i3wm": "i3",
    "kodi": "Kodi",
    "lxqt": "LXQt",
    "mate": "Mate",
    "phosh": "Phosh",
    "plasma-bigscreen": "Plasma Bigscreen",
    "plasma-desktop": "Plasma Desktop",
    "plasma-mobile": "Plasma Mobile",
    "shelli": "Shelli",
    "sway": "Sway",
    "sxmo-de-dwm": "Sxmo (dwm)",
    "sxmo-de-sway": "Sxmo (Sway)",
    "weston": "Weston",
    "xfce4": "XFCE4",
}

# Pretty name mapping for device codenames
devices = {
    "arrow-db410c": "Arrow DragonBoard 410c",
    "asus-me176c": "ASUS MeMO Pad 7",
    "bq-paella": "BQ Aquaris X5",
    "fairphone-fp4": "Fairphone 4",
    "fairphone-fp5": "Fairphone 5",
    "generic-x86_64": "Generic x86_64 Device",
    "google-asurada": "Google Asurada Chromebooks",
    "google-cherry": "Google Cherry Chromebooks",
    "google-corsola": "Google Corsola Chromebooks",
    "google-gru": "Google Gru Chromebooks",
    "google-kukui": "Google Kukui Chromebooks",
    "google-nyan-big": "Acer Chromebook 13 CB5-311",
    "google-nyan-blaze": "HP Chromebook 14 G3",
    "google-oak": "Google Oak Chromebooks",
    "google-peach-pi": "Samsung Chromebook 2 13.3\"",
    "google-peach-pit": "Samsung Chromebook 2 11.6\"",
    "google-sargo": "Google Pixel 3a",
    "google-snow": "Samsung Chromebook",
    "google-trogdor": "Google Trogdor Chromebooks",
    "google-veyron": "Google Veyron Chromebooks",
    "google-x64cros": "Google Chromebooks with x64 CPU",
    "lenovo-21bx": "Lenovo Thinkpad X13s",
    "lenovo-a6000": "Lenovo A6000",
    "lenovo-a6010": "Lenovo A6010",
    "microsoft-surface-rt": "Microsoft Surface RT",
    "motorola-harpia": "Motorola Moto G4 Play",
    "nokia-n900": "Nokia N900",
    "nvidia-tegra-armv7": "Nvidia Tegra armv7",
    "odroid-hc2": "ODROID HC2",
    "odroid-xu4": "ODROID XU4",
    "oneplus-enchilada": "OnePlus 6",
    "oneplus-fajita": "OnePlus 6T",
    "pine64-pinebookpro": "PINE64 Pinebook Pro",
    "pine64-pinephone": "PINE64 PinePhone",
    "pine64-pinephonepro": "PINE64 PinePhone Pro",
    "pine64-rockpro64": "PINE64 RockPro64",
    "postmarketos-trailblazer": "PostmarketOS Trailblazer",
    "purism-librem5": "Purism Librem5",
    "qemu-amd64": "QEMU amd64",  # just used for test suite
    "qcom-msm8953": "Generic Qualcomm MSM8953",
    "samsung-a3": "Samsung Galaxy A3 2015",
    "samsung-a5": "Samsung Galaxy A5 2015",
    "samsung-coreprimevelte": "Samsung Galaxy Core Prime VE LTE",
    "samsung-e7": "Samsung Galaxy E7",
    "samsung-espresso10": "Samsung Galaxy Tab 2 10.1\"",
    "samsung-espresso7": "Samsung Galaxy Tab 2 7.0\"",
    "samsung-grandmax": "Samsung Galaxy Grand Max",
    "samsung-gt510": "Samsung Galaxy Tab A 9.7 2015",
    "samsung-gt58": "Samsung Galaxy Tab A 8.0 2015",
    "samsung-m0": "Samsung Galaxy S III",
    "samsung-manta": "Google Nexus 10",
    "samsung-serranove": "Samsung Galaxy S4 Mini Value Edition",
    "shift-axolotl": "SHIFT SHIFT6mq",
    "wileyfox-crackling": "Wileyfox Swift",
    "xiaomi-beryllium": "Xiaomi POCO F1",
    "xiaomi-daisy": "Xiaomi Mi A2 Lite",
    "xiaomi-davinci": "Xiaomi Mi 9T / Redmi K20",
    "xiaomi-elish": "Xiaomi Mi Pad 5 Pro",
    "xiaomi-markw": "Xiaomi Redmi 4 Prime",
    "xiaomi-mido": "Xiaomi Redmi Note 4",
    "xiaomi-scorpio": "Xiaomi Mi Note 2",
    "xiaomi-surya": "Xiaomi POCO X3 NFC",
    "xiaomi-tissot": "Xiaomi Mi A1",
    "xiaomi-vince": "Xiaomi Redmi 5 Plus",
    "xiaomi-wt88047": "Xiaomi Redmi 2",
    "xiaomi-ysl": "Xiaomi Redmi S2/Y2",
}

# Build configuration, can be overridden per device/branch in 'images' below
branch_config_default = {
    # Schedule a new image each {date-interval} days, start at {date-start}.
    "date-interval": 7,
    "date-start": "2021-07-21",  # Wednesday

    # User interfaces to build. At least one UI must be set for each device,
    # otherwise no image for that device will be built.
    "ui": get_ui_list(chassis=["handset"]),

    # Build images with android recovery zip
    "android-recovery-zip": False,

    # To create additional images with other kernels selected, override this
    # variable. For qemu-amd64, this could be ["virt", "lts"].
    # https://postmarketos.org/multiple-kernels
    "kernels": [""],

    # How many images (image directories) to keep of one branch:device:ui
    # combination. Older images will get deleted to free up space.
    "keep": 3
}

# For each image you add here, make sure there is a proper wiki redirect for
# https://wiki.postmarketos.org/wiki/<codename>. That is what will show up in
# the generated readme.html!
images = {
    "arrow-db410c": {
        "branch_configs": {
            "all": {
                "ui": get_ui_list(chassis=["embedded"]),
            },
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "asus-me176c": {
        "branch_configs": {
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "bq-paella": {
        "branch_configs": {
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "fairphone-fp4": {
        "branch_configs": {
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "fairphone-fp5": {
        "branch_configs": {
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "generic-x86_64": {
        "branch_configs": {
            "all": {
                # Disable plasma-desktop:
                # https://gitlab.postmarketos.org/postmarketOS/build.postmarketos.org/-/issues/136
                "ui": get_ui_list(chassis=["laptop", "convertible", "tablet"],
                                  exclude_ui=["plasma-desktop"]),
                "kernels": [
                    "lts",
                ],
            },
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "google-asurada": {
        "branches": [
            "master",
            "v24.12",
        ],
        "branch_configs": {
            "all": {
                "ui": get_ui_list(chassis=["laptop", "convertible"]),
            },
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "google-cherry": {
        "branches": [
            "master",
            "v24.12",
        ],
        "branch_configs": {
            "all": {
                "ui": get_ui_list(chassis=["laptop", "convertible"]),
            },
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "google-corsola": {
        "branches": [
            "master",
            "v24.12",
        ],
        "branch_configs": {
            "all": {
                "ui": get_ui_list(chassis=["laptop", "convertible"]),
            },
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "google-gru": {
        "branch_configs": {
            "all": {
                "ui": get_ui_list(chassis=["laptop", "convertible", "tablet"]),
            },
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "google-kukui": {
        "branch_configs": {
            "all": {
                "ui": get_ui_list(chassis=["laptop", "convertible", "tablet"]),
            },
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "google-nyan-big": {
        "branches": [
            # Disabled on master:
            # https://gitlab.postmarketos.org/postmarketOS/pmaports/-/issues/3186
        ],
        "branch_configs": {
            "all": {
                "kernels": [
                    "nyan-big",
                    "nyan-big-fhd",
                ],
                "ui": get_ui_list(chassis=["laptop"], supports_gpu=False),
            },
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "google-nyan-blaze": {
        "branches": [
            # Disabled on master:
            # https://gitlab.postmarketos.org/postmarketOS/pmaports/-/issues/3186
        ],
        "branch_configs": {
            "all": {
                "ui": get_ui_list(chassis=["laptop"], supports_gpu=False),
            },
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "google-oak": {
        "branch_configs": {
            "all": {
                "ui": get_ui_list(chassis=["laptop", "convertible"], supports_gpu=False),
            },
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "google-peach-pi": {
        "branches": [
            "master",
            "v24.12",
        ],
        "branch_configs": {
            "all": {
                "ui": get_ui_list(chassis=["laptop"]),
            },
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "google-peach-pit": {
        "branch_configs": {
            "all": {
                "ui": get_ui_list(chassis=["laptop"]),
            },
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "google-sargo": {
        "branches": [
            "master",
            "v24.12",
        ],
    },
    "google-snow": {
        "branch_configs": {
            "master": {
                "ui": get_ui_list(chassis=["laptop"]),
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "google-trogdor": {
        "branch_configs": {
            "all": {
                "ui": get_ui_list(chassis=["laptop", "convertible", "tablet"]),
            },
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "google-veyron": {
        "branch_configs": {
            "all": {
                "ui": get_ui_list(chassis=["laptop", "convertible"]),
            },
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "google-x64cros": {
        "branch_configs": {
            "all": {
                "kernels": [
                    # "lts" kernel would be a bit better in theory, but it is
                    # too big to fit the cgpt_kpart partition:
                    #   % dd if=…/boot/vmlinuz.kpart of=/dev/installp1
                    #   dd: error writing '/dev/installp1': No space left on device
                    # Use "edge" instead, it actually misses only a few configs
                    # for these devices, nothing critical.
                    "edge",
                ],
                # Disable plasma-desktop:
                # https://gitlab.postmarketos.org/postmarketOS/build.postmarketos.org/-/issues/136
                "ui": get_ui_list(chassis=["laptop", "convertible"],
                                  exclude_ui=["plasma-desktop"]),
            },
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "lenovo-21bx": {
        "branch_configs": {
            "all": {
                "ui": get_ui_list(chassis=["laptop"]),
            },
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "lenovo-a6000": {
        "branch_configs": {
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "lenovo-a6010": {
        "branch_configs": {
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "microsoft-surface-rt": {
        "branch_configs": {
            "all": {
                # Tablet with detachable keyboard
                "ui": get_ui_list(chassis=["convertible"], supports_gpu=False),
            },
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "motorola-harpia": {
    },
    "nokia-n900": {
        "branch_configs": {
            "all": {
                # Handset with keyboard
                "ui": get_ui_list(chassis=["convertible"], supports_gpu=False),
            },
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "nvidia-tegra-armv7": {
        "branch_configs": {
            "all": {
                "ui": get_ui_list(chassis=["convertible", "tablet", "handset"], supports_gpu=False),
            },
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "odroid-xu4": {
        "branch_configs": {
            "all": {
                "ui": get_ui_list(chassis=["embedded"]),
            },
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "oneplus-enchilada": {
        "branch_configs": {
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "oneplus-fajita": {
        "branch_configs": {
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "pine64-pinebookpro": {
        "branch_configs": {
            "all": {
                "ui": get_ui_list(chassis=["laptop"]),
            },
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "pine64-pinephone": {
        "branch_configs": {
            "v24.12": {
                "date-start": "2024-12-20",  # Friday
            },
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "pine64-pinephonepro": {
    },
    "pine64-rockpro64": {
        "branch_configs": {
            # Disable plasma bigscreen:
            # https://gitlab.postmarketos.org/postmarketOS/pmaports/-/issues/2650
            "all": {
                "ui": get_ui_list(chassis=["embedded"]),
            },
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "postmarketos-trailblazer": {
        "branches": [
            "master",
            # Decided to not ship it in stable releases
        ],
        "branch_configs": {
            "master": {
                "date-interval": 1,
                "ui": ["gnome", "console"],
                "kernels": [
                    "next",
                ],
                "date-start": "2025-01-10",  # Friday
            },
        }
    },
    "purism-librem5": {
        "branch_configs": {
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        }
    },
    "qcom-msm8953": {
        "branches": [
            "master",
            "v24.12",
        ],
        "branch_configs": {
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        }
    },
    "samsung-a3": {
        "branch_configs": {
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        }
    },
    "samsung-a5": {
        "branch_configs": {
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        }
    },
    "samsung-coreprimevelte": {
        "branch_configs": {
            "all": {
                # Only usable UI at the moment
                "ui": [ "sxmo-de-sway" ],
            },
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "samsung-e7": {
        "branch_configs": {
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "samsung-espresso10": {
        "branch_configs": {
            "all": {
                "android-recovery-zip": True,
                "date-start": "2024-06-08",  # Saturday
                "ui": get_ui_list(chassis=["tablet"], supports_gpu=False),
            },
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "samsung-espresso7": {
        "branch_configs": {
            "all": {
                "android-recovery-zip": True,
                "date-start": "2024-06-08",  # Saturday
                "ui": get_ui_list(chassis=["tablet"], supports_gpu=False),
            },
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "samsung-grandmax": {
        "branch_configs": {
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "samsung-gt510": {
        "branch_configs": {
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "samsung-gt58": {
        "branch_configs": {
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "samsung-m0": {
        "branch_configs": {
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "samsung-manta": {
        "branch_configs": {
            "all": {
                "ui": get_ui_list(chassis=["tablet"]),
            },
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "samsung-serranove": {
        "branch_configs": {
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "shift-axolotl": {
        "branch_configs": {
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "wileyfox-crackling": {
        "branch_configs": {
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "xiaomi-beryllium": {
        "branch_configs": {
            "all": {
                "kernels": [
                    "tianma",
                    "ebbg",
                ],
            },
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "xiaomi-davinci": {
        "branches": [
            "master",
        ],
        "branch_configs": {
            "master": {
                "date-start": "2025-02-21",  # Friday
            },
        },
    },
    "xiaomi-elish": {
        "branches": [
            "master",
            "v24.12",
        ],
        "branch_configs": {
            "all": {
                "kernels": [
                    "boe",
                    "csot",
                ],
            },
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "xiaomi-scorpio": {
        "branch_configs": {
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
    "xiaomi-surya": {
        "branches": [
            "master",
        ],
        "branch_configs": {
            "all": {
                "kernels": [
                    "huaxing",
                    "tianma",
                ],
            },
            "master": {
                "date-start": "2025-02-21",  # Friday
            },
        },
    },
    "xiaomi-wt88047": {
        "branch_configs": {
            "master": {
                "date-start": "2025-01-10",  # Friday
            },
        },
    },
}
